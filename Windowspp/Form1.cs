﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Windowspp
{
          public partial class Form1 : Form
          {
                    public List<Apartmani> apartmani = new List<Apartmani>();
                    public Form1()
                    {
                              InitializeComponent();
                    }

                    private void button1_Click(object sender, EventArgs e)
                    {
                              
                              string URLPost = "http://localhost:64736/api/booking/post";
                              if (txtGost.Text != "" && cmbObjekt.Text != "" && cmbSoba.Text != "" && cmbSoba.Text.All(char.IsDigit) == true && dtpDolazak.Value<dtpOdlazak.Value)
                              {
                                        string[] niz = PrilagodiTekst(txtGost.Text, cmbObjekt.Text, cmbSoba.Text, dtpDolazak.Value.ToString("yyyy-MM-ddTHH:mm"), dtpOdlazak.Value.ToString("yyyy-MM-ddTHH:mm"));
                                        string rezervacija = "{'Gost':" + niz[0] + ",'ImeObjekta':" + niz[1] + ",'BrojSobe':" + Int32.Parse(niz[2]) + ",'DatumDolaska':" + niz[3] + ",'DatumOdlaska':" + niz[4] + "}";
                                        lstRezervacije.Items.Clear();
                                        var data = webPostMethod(rezervacija, URLPost);
                                        string URLGet = "http://localhost:64736/api/booking";
                                        var getData = webGetMethod(URLGet);
                                        dynamic deserialized = JsonConvert.DeserializeObject(getData);
                                        for (int i = 0; i < deserialized.Count; i++)
                                        {
                                                  int duljinaDolazak = deserialized[i].DatumDolaska.ToString().Length;
                                                  int duljinaOdlazak = deserialized[i].DatumOdlaska.ToString().Length;
                                                  lstRezervacije.Items.Add(deserialized[i].Id + "- Gost= " + deserialized[i].Gost + " ; " + deserialized[i].ImeObjekta + " ; Broj sobe= " + deserialized[i].BrojSobe + " ; Datum dolaska= " + deserialized[i].DatumDolaska.ToString().Remove(duljinaDolazak - 3) + " ; Datum odlaska= " + deserialized[i].DatumOdlaska.ToString().Remove(duljinaOdlazak - 3));
                                        }
                                        MessageBox.Show("Rezervacija uspješna");
                              }
                              else 
                              {
                                        MessageBox.Show("Ispunite sva polja valjanim vrijednostima!");
                              }

                    }

                    private void label1_Click(object sender, EventArgs e)
                    {

                    }                   
                    private void Form1_Load(object sender, EventArgs e)
                    {


                              //var client2 = new HttpClient();
                              //client2.BaseAddress = new Uri("http://localhost:64736");
                              //HttpResponseMessage response2 = await client2.GetAsync("api/booking/send");
                              //string result2 = await response2.Content.ReadAsStringAsync();
                              //dynamic deserialized2 = JsonConvert.DeserializeObject(result2);
                              //Apartmani a = new Apartmani();
                              //for (int i = 0; i < deserialized2.Count; i++)
                              //{
                              //          a.ImeObjekta = deserialized2[i].ImeObjekta;
                              //          a.BrojSoba= deserialized2[i].BrojSoba;
                              //          apartmani.Add(a);
                              //          cmbObjekt.Items.Add(apartmani[i].ImeObjekta);
                              //          cmbSoba.Items.Add(apartmani[i].BrojSoba);

                              //}
                              Apartmani prvi = new Apartmani();
                              prvi.ImeObjekta = "Apartman 1";
                              prvi.BrojSoba = 5;
                              Apartmani drugi = new Apartmani();
                              drugi.ImeObjekta = "Apartman 2";
                              drugi.BrojSoba = 4;
                              Apartmani treci = new Apartmani();
                              treci.ImeObjekta = "Apartman 3";
                              treci.BrojSoba = 6;
                              apartmani.Add(prvi);
                              apartmani.Add(drugi);
                              apartmani.Add(treci);
                              for (int i=0; i < apartmani.Count; i++)
                              {
                                        cmbObjekt.Items.Add(apartmani[i].ImeObjekta);
                                        
                              }
                              cmbObjekt.DropDownStyle = ComboBoxStyle.DropDownList;
                              cmbSoba.DropDownStyle = ComboBoxStyle.DropDownList;


                              //var client = new HttpClient();
                              //client.BaseAddress = new Uri("http://localhost:64736/");
                              //HttpResponseMessage response = await client.GetAsync("api/booking");
                              //string result = await response.Content.ReadAsStringAsync();
                              string result = webGetMethod("http://localhost:64736/api/booking");
                              dynamic deserialized = JsonConvert.DeserializeObject(result);                              
                              for (int i=0;i<deserialized.Count;i++)
                              {
                                  int duljinaDolazak = deserialized[i].DatumDolaska.ToString().Length;
                                  int duljinaOdlazak= deserialized[i].DatumOdlaska.ToString().Length;
                                  lstRezervacije.Items.Add(deserialized[i].Id+"- Gost= "+deserialized[i].Gost+" ; "+deserialized[i].ImeObjekta+" ; Broj sobe= "+deserialized[i].BrojSobe + " ; Datum dolaska= "+ deserialized[i].DatumDolaska.ToString().Remove(duljinaDolazak-3) + " ; Datum odlaska= " + deserialized[i].DatumOdlaska.ToString().Remove(duljinaOdlazak - 3));
                              }                             

                    }
                    private void btnIzmijeni_Click(object sender, EventArgs e)
                    {
                              txtGost.Text = "";
                              cmbSoba.Items.Clear();
                              if (lstRezervacije.SelectedIndex > -1)
                              {
                                        cmbObjekt.DropDownStyle = ComboBoxStyle.DropDown;
                                        cmbSoba.DropDownStyle = ComboBoxStyle.DropDown;


                                        var odabrano = lstRezervacije.SelectedItem.ToString();
                                        string[] niz = odabrano.Split(';');
                                        string[] gost = niz[0].Split('=');
                                        string imegosta = gost[1];
                                        string[] datumDolazak = niz[3].Split('=');
                                        string datumD = datumDolazak[1];
                                        string[] konacandatumDolazak = datumD.Split('.');
                                        int year = Int32.Parse(konacandatumDolazak[2]);
                                        int day = Int32.Parse(konacandatumDolazak[0]);
                                        int month = Int32.Parse(konacandatumDolazak[1]);
                                        string[] time = konacandatumDolazak[3].Split(':');
                                        int hour = Int32.Parse(time[0]);
                                        int min = Int32.Parse(time[1]);
                                        string[] datumOdlazak = niz[4].Split('=');
                                        string datumO = datumOdlazak[1];
                                        string[] konacandatumOdlazak = datumO.Split('.');
                                        int yearO = Int32.Parse(konacandatumOdlazak[2]);
                                        int dayO = Int32.Parse(konacandatumOdlazak[0]);
                                        int monthO = Int32.Parse(konacandatumOdlazak[1]);
                                        string[] timeO = konacandatumOdlazak[3].Split(':');
                                        int hourO = Int32.Parse(timeO[0]);
                                        int minO = Int32.Parse(timeO[1]);
                                        txtGost.Text = imegosta;
                                        string obj = niz[1];
                                        string[] brojSobe = niz[2].Split('=');
                                        string soba = brojSobe[1];
                                        cmbObjekt.Text = obj;
                                        cmbSoba.Text = soba;
                                        string objekt = cmbObjekt.Text.Trim();
                                        foreach (var item in apartmani)
                                        {
                                                  if (objekt == item.ImeObjekta)
                                                  {
                                                            for (int i = 1; i <= item.BrojSoba; i++)
                                                            {
                                                                      cmbSoba.Items.Add(i);
                                                            }
                                                  }
                                        }
                                        var date = new DateTime(year, month, day, hour, min, 0);
                                        var date2 = new DateTime(yearO, monthO, dayO, hourO, minO, 0);
                                        dtpDolazak.Value = date;
                                        dtpOdlazak.Value = date2;
                                        //cmbObjekt.DropDownStyle = ComboBoxStyle.DropDown;
                                        //cmbSoba.DropDownStyle = ComboBoxStyle.DropDownList;
                              }
                              else if (lstRezervacije.Items.Count == 0)
                              {
                                        MessageBox.Show("Trenutno nema rezervacija");
                              }
                              else
                              {
                                        MessageBox.Show("Izaberite jednu od rezervacija na listi koju želite izmijeniti");
                              }



                    }
                    public string webPostMethod(string postData, string URL)
                    {
                              string responseFromServer = "";
                              HttpWebRequest request = (HttpWebRequest)WebRequest.Create(URL);
                              request.Method = "POST";
                              request.Credentials = CredentialCache.DefaultCredentials;
                              ((HttpWebRequest)request).UserAgent =
                                                "Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 7.1; Trident/5.0)";
                              request.Accept = "/";
                              request.UseDefaultCredentials = true;
                              request.Proxy.Credentials = System.Net.CredentialCache.DefaultCredentials;
                              byte[] byteArray = Encoding.UTF8.GetBytes(postData);
                              request.ContentType = "application/json";
                              request.ContentLength = byteArray.Length;
                              Stream dataStream = request.GetRequestStream();
                              dataStream.Write(byteArray, 0, byteArray.Length);
                              dataStream.Close();

                              WebResponse response = request.GetResponse();
                              dataStream = response.GetResponseStream();
                              StreamReader reader = new StreamReader(dataStream);
                              responseFromServer = reader.ReadToEnd();
                              reader.Close();
                              dataStream.Close();
                              response.Close();
                              return responseFromServer;
                    }
                    public string webGetMethod(string URL)
                    {
                              string jsonString = "";
                              HttpWebRequest request = (HttpWebRequest)WebRequest.Create(URL);
                              request.Method = "GET";
                              request.Credentials = CredentialCache.DefaultCredentials;
                              ((HttpWebRequest)request).UserAgent = "Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 7.1; Trident/5.0)";
                              request.Accept = "/";
                              request.UseDefaultCredentials = true;
                              request.Proxy.Credentials = System.Net.CredentialCache.DefaultCredentials;
                              request.ContentType = "application/json";
                              WebResponse response = request.GetResponse();
                              StreamReader sr = new StreamReader(response.GetResponseStream());
                              jsonString = sr.ReadToEnd();
                              sr.Close();
                              return jsonString;
                    }
                    public string[] PrilagodiTekst(string gost, string objekt, string soba, string datumD, string datumO)
                    {
                              string noviGost = gost.Trim();
                              string noviGost2 = noviGost.Insert(0, "'");
                              string noviGost3 = noviGost2.Insert(noviGost2.Length, "'");
                              string noviObjekt = objekt.Trim();
                              string noviObjekt2 = noviObjekt.Insert(0, "'");
                              string noviObjekt3 = noviObjekt2.Insert(noviObjekt2.Length, "'");
                              int novaSoba = Int32.Parse(soba.Trim());
                              string datumDolazak = datumD;
                              string datumOdlazak = datumO;
                              string datumDolazak2 = datumDolazak.Insert(0, "'");
                              string datumDolazak3 = datumDolazak2.Insert(datumDolazak2.Length, "'");
                              string datumOdlazak2 = datumOdlazak.Insert(0, "'");
                              string datumOdlazak3 = datumOdlazak2.Insert(datumOdlazak2.Length, "'");
                              string[] niz = { noviGost3, noviObjekt3, novaSoba.ToString(), datumDolazak3, datumOdlazak3 };
                              return niz;
                    }
                    private void btnPromjeni_Click(object sender, EventArgs e)
                    {
                              if (lstRezervacije.SelectedIndex > -1)
                              {                                        
                                        var text = lstRezervacije.SelectedItem.ToString();                                        
                                        string[] niz = text.Split('-');
                                        string index = niz[0];
                                        lstRezervacije.Items.Clear();
                                        int noviindex = Int32.Parse(index);                                        
                                        string[] nizprilagodi = PrilagodiTekst(txtGost.Text, cmbObjekt.Text, cmbSoba.Text, dtpDolazak.Value.ToString("yyyy-MM-ddTHH:mm"), dtpOdlazak.Value.ToString("yyyy-MM-ddTHH:mm"));
                                        string rezervacija = "{'Id':" + noviindex + ",'Gost':" + nizprilagodi[0] + ",'ImeObjekta':"+nizprilagodi[1]+",'BrojSobe':" + Int32.Parse(nizprilagodi[2]) + ",'DatumDolaska':"+nizprilagodi[3]+",'DatumOdlaska':"+nizprilagodi[4]+"}";
                                        string URL = "http://localhost:64736/api/booking/update";
                                        string URLGet = "http://localhost:64736/api/booking";
                                        var data = webPostMethod(rezervacija, URL);
                                        var getData = webGetMethod(URLGet);
                                        dynamic deserialized = JsonConvert.DeserializeObject(getData);                                        
                                        for (int i = 0; i < deserialized.Count; i++)
                                        {
                                                  int duljinaDolazak = deserialized[i].DatumDolaska.ToString().Length;
                                                  int duljinaOdlazak = deserialized[i].DatumOdlaska.ToString().Length;
                                                  lstRezervacije.Items.Add(deserialized[i].Id + "- Gost= " + deserialized[i].Gost + " ; " + deserialized[i].ImeObjekta + " ; Broj sobe= " + deserialized[i].BrojSobe + " ; Datum dolaska= " + deserialized[i].DatumDolaska.ToString().Remove(duljinaDolazak - 3) + " ; Datum odlaska= " + deserialized[i].DatumOdlaska.ToString().Remove(duljinaOdlazak - 3));
                                        }
                              }
                              else if (lstRezervacije.Items.Count==0)
                              {
                                        MessageBox.Show("Trenutno nema rezervacija");
                              }
                              else
                              {
                                        MessageBox.Show("Izaberite jednu od rezervacija na listi koju želite izmijeniti");
                              }



                    }

                    private void btnIzbrisi_Click(object sender, EventArgs e)
                    {
                              if (lstRezervacije.SelectedIndex > -1)
                              {
                                        string URL = "http://localhost:64736/api/booking/delete";
                                        var text = lstRezervacije.SelectedItem.ToString();
                                        lstRezervacije.Items.Clear();
                                        string[] niz = text.Split('-');
                                        string brisi = niz[0];
                                        var data = webPostMethod(brisi, URL);
                                        string URLGet = "http://localhost:64736/api/booking";
                                        var getData = webGetMethod(URLGet);
                                        dynamic deserialized = JsonConvert.DeserializeObject(getData);                                        
                                        for (int i = 0; i < deserialized.Count; i++)
                                        {
                                                  int duljinaDolazak = deserialized[i].DatumDolaska.ToString().Length;
                                                  int duljinaOdlazak = deserialized[i].DatumOdlaska.ToString().Length;
                                                  lstRezervacije.Items.Add(deserialized[i].Id + "- Gost= " + deserialized[i].Gost + " ; " + deserialized[i].ImeObjekta + " ; Broj sobe= " + deserialized[i].BrojSobe + " ; Datum dolaska= " + deserialized[i].DatumDolaska.ToString().Remove(duljinaDolazak - 3) + " ; Datum odlaska= " + deserialized[i].DatumOdlaska.ToString().Remove(duljinaOdlazak - 3));
                                        }
                              }
                              else if (lstRezervacije.Items.Count == 0)
                              {
                                        MessageBox.Show("Trenutno nema rezervacija");
                              }
                              else
                              {
                                        MessageBox.Show("Izaberite jednu od rezervacija na listi koju želite izbrisati");
                              }

                    }

                    private void cmbObjekt_SelectedIndexChanged(object sender, EventArgs e)
                    {
                              string objekt = cmbObjekt.SelectedItem.ToString();
                              cmbSoba.Items.Clear();
                              cmbSoba.Text = "";
                              foreach (var item in apartmani)
                              {
                                        if (objekt == item.ImeObjekta)
                                        {
                                           for (int i=1;i<=item.BrojSoba;i++)
                                           {
                                                            cmbSoba.Items.Add(i);
                                           }
                                        }
                              }

                    }
          }
}
