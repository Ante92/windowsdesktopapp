﻿namespace Windowspp
{
          partial class Form1
          {
                    /// <summary>
                    /// Required designer variable.
                    /// </summary>
                    private System.ComponentModel.IContainer components = null;

                    /// <summary>
                    /// Clean up any resources being used.
                    /// </summary>
                    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
                    protected override void Dispose(bool disposing)
                    {
                              if (disposing && (components != null))
                              {
                                        components.Dispose();
                              }
                              base.Dispose(disposing);
                    }

                    #region Windows Form Designer generated code

                    /// <summary>
                    /// Required method for Designer support - do not modify
                    /// the contents of this method with the code editor.
                    /// </summary>
                    private void InitializeComponent()
                    {
                              this.cmbObjekt = new System.Windows.Forms.ComboBox();
                              this.cmbSoba = new System.Windows.Forms.ComboBox();
                              this.btnNovaRezervacija = new System.Windows.Forms.Button();
                              this.txtGost = new System.Windows.Forms.TextBox();
                              this.label1 = new System.Windows.Forms.Label();
                              this.label2 = new System.Windows.Forms.Label();
                              this.label3 = new System.Windows.Forms.Label();
                              this.btnIzmijeni = new System.Windows.Forms.Button();
                              this.dtpDolazak = new System.Windows.Forms.DateTimePicker();
                              this.dtpOdlazak = new System.Windows.Forms.DateTimePicker();
                              this.label4 = new System.Windows.Forms.Label();
                              this.label5 = new System.Windows.Forms.Label();
                              this.lstRezervacije = new System.Windows.Forms.ListBox();
                              this.btnIzbrisi = new System.Windows.Forms.Button();
                              this.btnPromjeni = new System.Windows.Forms.Button();
                              this.SuspendLayout();
                              // 
                              // cmbObjekt
                              // 
                              this.cmbObjekt.FormattingEnabled = true;
                              this.cmbObjekt.Location = new System.Drawing.Point(403, 56);
                              this.cmbObjekt.Name = "cmbObjekt";
                              this.cmbObjekt.Size = new System.Drawing.Size(121, 21);
                              this.cmbObjekt.TabIndex = 2;
                              this.cmbObjekt.SelectedIndexChanged += new System.EventHandler(this.cmbObjekt_SelectedIndexChanged);
                              // 
                              // cmbSoba
                              // 
                              this.cmbSoba.FormattingEnabled = true;
                              this.cmbSoba.Location = new System.Drawing.Point(403, 89);
                              this.cmbSoba.Name = "cmbSoba";
                              this.cmbSoba.Size = new System.Drawing.Size(121, 21);
                              this.cmbSoba.TabIndex = 3;
                              // 
                              // btnNovaRezervacija
                              // 
                              this.btnNovaRezervacija.Location = new System.Drawing.Point(61, 201);
                              this.btnNovaRezervacija.Name = "btnNovaRezervacija";
                              this.btnNovaRezervacija.Size = new System.Drawing.Size(162, 59);
                              this.btnNovaRezervacija.TabIndex = 4;
                              this.btnNovaRezervacija.Text = "Nova rezervacija";
                              this.btnNovaRezervacija.UseVisualStyleBackColor = true;
                              this.btnNovaRezervacija.Click += new System.EventHandler(this.button1_Click);
                              // 
                              // txtGost
                              // 
                              this.txtGost.Location = new System.Drawing.Point(403, 21);
                              this.txtGost.Name = "txtGost";
                              this.txtGost.Size = new System.Drawing.Size(121, 20);
                              this.txtGost.TabIndex = 5;
                              // 
                              // label1
                              // 
                              this.label1.AutoSize = true;
                              this.label1.Location = new System.Drawing.Point(348, 28);
                              this.label1.Name = "label1";
                              this.label1.Size = new System.Drawing.Size(32, 13);
                              this.label1.TabIndex = 7;
                              this.label1.Text = "Gost:";
                              this.label1.Click += new System.EventHandler(this.label1_Click);
                              // 
                              // label2
                              // 
                              this.label2.AutoSize = true;
                              this.label2.Location = new System.Drawing.Point(348, 59);
                              this.label2.Name = "label2";
                              this.label2.Size = new System.Drawing.Size(41, 13);
                              this.label2.TabIndex = 8;
                              this.label2.Text = "Objekt:";
                              // 
                              // label3
                              // 
                              this.label3.AutoSize = true;
                              this.label3.Location = new System.Drawing.Point(348, 92);
                              this.label3.Name = "label3";
                              this.label3.Size = new System.Drawing.Size(35, 13);
                              this.label3.TabIndex = 9;
                              this.label3.Text = "Soba:";
                              // 
                              // btnIzmijeni
                              // 
                              this.btnIzmijeni.Location = new System.Drawing.Point(364, 189);
                              this.btnIzmijeni.Name = "btnIzmijeni";
                              this.btnIzmijeni.Size = new System.Drawing.Size(114, 38);
                              this.btnIzmijeni.TabIndex = 10;
                              this.btnIzmijeni.Text = "Izaberi rezervaciju koju želiš izmijeniti";
                              this.btnIzmijeni.UseVisualStyleBackColor = true;
                              this.btnIzmijeni.Click += new System.EventHandler(this.btnIzmijeni_Click);
                              // 
                              // dtpDolazak
                              // 
                              this.dtpDolazak.CustomFormat = "dd/MM/yyyy HH:mm";
                              this.dtpDolazak.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
                              this.dtpDolazak.Location = new System.Drawing.Point(403, 125);
                              this.dtpDolazak.Name = "dtpDolazak";
                              this.dtpDolazak.Size = new System.Drawing.Size(138, 20);
                              this.dtpDolazak.TabIndex = 11;
                              this.dtpDolazak.Value = new System.DateTime(2019, 10, 4, 0, 0, 0, 0);
                              // 
                              // dtpOdlazak
                              // 
                              this.dtpOdlazak.CustomFormat = "dd/MM/yyyy HH:mm";
                              this.dtpOdlazak.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
                              this.dtpOdlazak.Location = new System.Drawing.Point(403, 158);
                              this.dtpOdlazak.Name = "dtpOdlazak";
                              this.dtpOdlazak.Size = new System.Drawing.Size(138, 20);
                              this.dtpOdlazak.TabIndex = 12;
                              this.dtpOdlazak.Value = new System.DateTime(2019, 10, 2, 0, 0, 0, 0);
                              // 
                              // label4
                              // 
                              this.label4.AutoSize = true;
                              this.label4.Location = new System.Drawing.Point(316, 125);
                              this.label4.Name = "label4";
                              this.label4.Size = new System.Drawing.Size(81, 13);
                              this.label4.TabIndex = 13;
                              this.label4.Text = "Datum dolaska:";
                              // 
                              // label5
                              // 
                              this.label5.AutoSize = true;
                              this.label5.Location = new System.Drawing.Point(316, 157);
                              this.label5.Name = "label5";
                              this.label5.Size = new System.Drawing.Size(81, 13);
                              this.label5.TabIndex = 14;
                              this.label5.Text = "Datum odlaska:";
                              // 
                              // lstRezervacije
                              // 
                              this.lstRezervacije.FormattingEnabled = true;
                              this.lstRezervacije.HorizontalScrollbar = true;
                              this.lstRezervacije.Location = new System.Drawing.Point(12, 12);
                              this.lstRezervacije.Name = "lstRezervacije";
                              this.lstRezervacije.Size = new System.Drawing.Size(298, 173);
                              this.lstRezervacije.TabIndex = 15;
                              // 
                              // btnIzbrisi
                              // 
                              this.btnIzbrisi.Location = new System.Drawing.Point(515, 189);
                              this.btnIzbrisi.Name = "btnIzbrisi";
                              this.btnIzbrisi.Size = new System.Drawing.Size(106, 38);
                              this.btnIzbrisi.TabIndex = 16;
                              this.btnIzbrisi.Text = "Izbriši rezervaciju";
                              this.btnIzbrisi.UseVisualStyleBackColor = true;
                              this.btnIzbrisi.Click += new System.EventHandler(this.btnIzbrisi_Click);
                              // 
                              // btnPromjeni
                              // 
                              this.btnPromjeni.Location = new System.Drawing.Point(364, 233);
                              this.btnPromjeni.Name = "btnPromjeni";
                              this.btnPromjeni.Size = new System.Drawing.Size(114, 36);
                              this.btnPromjeni.TabIndex = 17;
                              this.btnPromjeni.Text = "Potvrdi promjene";
                              this.btnPromjeni.UseVisualStyleBackColor = true;
                              this.btnPromjeni.Click += new System.EventHandler(this.btnPromjeni_Click);
                              // 
                              // Form1
                              // 
                              this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
                              this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
                              this.ClientSize = new System.Drawing.Size(645, 281);
                              this.Controls.Add(this.btnPromjeni);
                              this.Controls.Add(this.btnIzbrisi);
                              this.Controls.Add(this.lstRezervacije);
                              this.Controls.Add(this.label5);
                              this.Controls.Add(this.label4);
                              this.Controls.Add(this.dtpOdlazak);
                              this.Controls.Add(this.dtpDolazak);
                              this.Controls.Add(this.btnIzmijeni);
                              this.Controls.Add(this.label3);
                              this.Controls.Add(this.label2);
                              this.Controls.Add(this.label1);
                              this.Controls.Add(this.txtGost);
                              this.Controls.Add(this.btnNovaRezervacija);
                              this.Controls.Add(this.cmbSoba);
                              this.Controls.Add(this.cmbObjekt);
                              this.Name = "Form1";
                              this.Text = "Form1";
                              this.Load += new System.EventHandler(this.Form1_Load);
                              this.ResumeLayout(false);
                              this.PerformLayout();

                    }

                    #endregion
                    private System.Windows.Forms.ComboBox cmbObjekt;
                    private System.Windows.Forms.ComboBox cmbSoba;
                    private System.Windows.Forms.Button btnNovaRezervacija;
                    private System.Windows.Forms.TextBox txtGost;
                    private System.Windows.Forms.Label label1;
                    private System.Windows.Forms.Label label2;
                    private System.Windows.Forms.Label label3;
                    private System.Windows.Forms.Button btnIzmijeni;
                    private System.Windows.Forms.DateTimePicker dtpDolazak;
                    private System.Windows.Forms.DateTimePicker dtpOdlazak;
                    private System.Windows.Forms.Label label4;
                    private System.Windows.Forms.Label label5;
                    private System.Windows.Forms.ListBox lstRezervacije;
                    private System.Windows.Forms.Button btnIzbrisi;
                    private System.Windows.Forms.Button btnPromjeni;
          }
}

